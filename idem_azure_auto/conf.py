DYNE = {
    "pop_create": ["autogen"],
    "tool": ["tool"],
    "states": ["states"],
    "exec": ["exec"],
    "acct": ["acct"],
}
SUBCOMMANDS = {
    "azure": {
        "help": "Create idem_azure state modules by Azure REST API web",
        "dyne": "pop_create",
    },
}
