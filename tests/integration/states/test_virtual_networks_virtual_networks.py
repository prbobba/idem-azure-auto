import uuid

import pytest


@pytest.mark.asyncio
async def test_virtual_network(hub, ctx, resource_group_fixture):
    """
    This test provisions a virtual network, describes virtual network, does a force update and deletes
     the provisioned virtual network.
    """
    # Create virtual network
    resource_group_name = resource_group_fixture.get("name")
    virtual_network_name = "idem-test-virtual-network-" + str(uuid.uuid4())
    vnet_parameters = {
        "location": "eastus",
        "properties": {
            "addressSpace": {"addressPrefixes": ["10.12.14.0/30"]},
            "flowTimeoutInMinutes": 10,
        },
    }
    vnet_ret = await hub.states.azure.virtual_networks.virtual_networks.present(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        parameters=vnet_parameters,
    )
    assert vnet_ret["result"], vnet_ret["comment"]
    assert not vnet_ret["changes"].get("old") and vnet_ret["changes"]["new"]

    vnet_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
    hub.tool.azure.test_utils.check_response_payload(
        vnet_parameters, vnet_wait_ret["ret"]
    )

    # Describe virtual network
    describe_ret = await hub.states.azure.virtual_networks.virtual_networks.describe(
        ctx
    )
    assert vnet_wait_ret["ret"].get("id") in describe_ret

    # Force updating virtual network
    vnet_force_update_parameters = {
        "location": "eastus",
        "properties": {
            "addressSpace": {"addressPrefixes": ["10.12.15.0/30"]},
            "flowTimeoutInMinutes": 10,
        },
    }
    vnet_force_update_ret = (
        await hub.states.azure.virtual_networks.virtual_networks.present(
            ctx,
            name=virtual_network_name,
            resource_group_name=resource_group_name,
            virtual_network_name=virtual_network_name,
            parameters=vnet_force_update_parameters,
            **{"force_update": True},
        )
    )
    assert vnet_force_update_ret["result"], vnet_force_update_ret["comment"]
    assert (
        vnet_force_update_ret["changes"].get("old")
        and vnet_force_update_ret["changes"]["new"]
    )

    vnet_force_update_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
    hub.tool.azure.test_utils.check_response_payload(
        vnet_force_update_parameters, vnet_force_update_wait_ret["ret"]
    )

    # Delete virtual network
    vnet_del_ret = await hub.states.azure.virtual_networks.virtual_networks.absent(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["changes"]["old"] and not vnet_del_ret["changes"].get("new")
    await hub.tool.azure.test_utils.wait_for_resource_to_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )
