import uuid

import pytest


@pytest.mark.asyncio
async def test_resource_group(hub, ctx):
    """
    This test provisions a resource group, describes resource group and deletes the provisioned resource group.
    """
    # Create resource group
    resource_group_name = "idem-test-resource-group-" + str(uuid.uuid4())
    rg_parameters = {"location": "eastus"}
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        parameters=rg_parameters,
    )
    assert rg_ret["result"], rg_ret["comment"]
    assert not rg_ret["changes"].get("old") and rg_ret["changes"].get("new")

    rg_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"?api-version=2021-04-01",
        retry_count=5,
        retry_period=10,
    )
    hub.tool.azure.test_utils.check_response_payload(rg_parameters, rg_wait_ret["ret"])

    # Describe resource group
    describe_ret = await hub.states.azure.resource_management.resource_groups.describe(
        ctx
    )
    assert rg_wait_ret["ret"].get("id") in describe_ret

    # Patch resource group
    rg_patch_parameters = {
        "location": "eastus",
        "tags": {
            "idem-test-tag-key-"
            + str(uuid.uuid4()): "idem-test-tag-value-"
            + str(uuid.uuid4())
        },
    }
    rg_patch_ret = await hub.states.azure.resource_management.resource_groups.present(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        parameters=rg_patch_parameters,
    )
    assert rg_patch_ret["result"], rg_patch_ret["comment"]
    assert not rg_patch_ret["changes"].get("old") and (
        rg_patch_ret["changes"].get("new").get("tags") == rg_patch_parameters["tags"]
    )

    rg_patch_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"?api-version=2021-04-01",
        retry_count=5,
        retry_period=10,
    )
    hub.tool.azure.test_utils.check_response_payload(
        rg_patch_parameters, rg_patch_wait_ret["ret"]
    )

    # Patch resource group to remove tag
    rg_patch_no_tag_parameters = {"location": "eastus", "tags": {}}
    rg_patch_no_tag_ret = (
        await hub.states.azure.resource_management.resource_groups.present(
            ctx,
            name=resource_group_name,
            resource_group_name=resource_group_name,
            parameters=rg_patch_no_tag_parameters,
        )
    )
    assert rg_patch_no_tag_ret["result"], rg_patch_no_tag_ret["comment"]
    assert rg_patch_no_tag_ret["changes"].get("old") and rg_patch_no_tag_ret[
        "changes"
    ].get("new")

    rg_patch_no_tag_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"?api-version=2021-04-01",
        retry_count=5,
        retry_period=10,
    )
    assert rg_patch_no_tag_wait_ret["ret"].get("tags", dict()) == dict()

    # Delete resource group
    rg_del_ret = await hub.states.azure.resource_management.resource_groups.absent(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
    )
    assert rg_del_ret["result"], rg_del_ret["comment"]
    assert rg_del_ret["changes"]["old"] and not rg_del_ret["changes"].get("new")
    await hub.tool.azure.test_utils.wait_for_resource_to_absent(
        ctx,
        url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
        f"?api-version=2021-04-01",
        retry_count=10,
        retry_period=10,
    )
