import asyncio
import copy
import uuid
from typing import Any
from typing import Dict
from typing import List
from unittest import mock

import dict_tools
import pytest

from tests import api_data as data


@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["azure"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_azure"


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    for dyne in ["idem", "pop_create"]:
        hub.pop.sub.add(dyne_name=dyne)

    with mock.patch("sys.argv", ["idem"]):
        hub.pop.config.load(["idem", "pop_create", "acct"], cli="idem")

    yield hub


@pytest.fixture(scope="session", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)

    # Add the profile to the account
    await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    yield ctx


async def _setup_hub(hub):
    # Set up the hub before each function here
    ...


async def _teardown_hub(hub):
    # Clean up the hub after each function here
    ...


@pytest.fixture(scope="function", autouse=True)
async def function_hub_wrapper(hub):
    await _setup_hub(hub)
    yield
    await _teardown_hub(hub)


@pytest.fixture(scope="function")
def url(hub):
    url = copy.deepcopy(data.URL)
    return url


@pytest.fixture(scope="function")
def desc(hub):
    desc = copy.deepcopy(data.DESC)
    return desc


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def resource_group_fixture(hub, ctx) -> Dict[str, Any]:
    """
    Fixture to create a new resource group. This resource group gets deleted during test clean-up to ensure that no
    testing resources get left over.
    """
    resource_group_name = "idem-test-resource-group-" + str(uuid.uuid4())
    parameters = {"location": "eastus"}
    rg_ret = await hub.states.azure.resource_management.resource_groups.present(
        ctx,
        name=resource_group_name,
        resource_group_name=resource_group_name,
        parameters=parameters,
    )
    if rg_ret["result"]:
        rg_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
            ctx,
            url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}?api-version=2021-04-01",
            retry_count=5,
            retry_period=5,
        )
        yield rg_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request resource group {resource_group_name} with parameters {parameters} with "
            f"error: {rg_ret['comment']} {rg_ret['ret']}"
        )

    delete_ret = await hub.states.azure.resource_management.resource_groups.absent(
        ctx, name=resource_group_name, resource_group_name=resource_group_name
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete resource group {resource_group_name}")
    else:
        hub.log.debug(
            f"Failed to delete resource group {resource_group_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="session")
async def virtual_network_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new virtual network. This virtual network gets deleted during test clean-up.
    """
    virtual_network_name = "idem-test-virtual-network-" + str(uuid.uuid4())
    resource_group_name = resource_group_fixture.get("name")
    vnet_parameters = {
        "location": "eastus",
        "properties": {
            "addressSpace": {"addressPrefixes": ["10.12.13.0/26"]},
            "flowTimeoutInMinutes": 10,
        },
    }
    vnet_ret = await hub.states.azure.virtual_networks.virtual_networks.present(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        parameters=vnet_parameters,
    )
    if vnet_ret["result"]:
        vnet_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
            ctx,
            url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
            f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield vnet_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request virtual network {virtual_network_name} with parameters {vnet_parameters} with "
            f"error: {vnet_ret['comment']} {vnet_ret['ret']}"
        )
    delete_ret = await hub.states.azure.virtual_networks.virtual_networks.absent(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete virtual network {virtual_network_name}")
    else:
        hub.log.debug(
            f"Failed to delete virtual network {virtual_network_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="session")
async def subnet_fixture(hub, ctx, resource_group_fixture, virtual_network_fixture):
    """
    Fixture to create a new subnet. This subnet gets deleted during test clean-up.
    """
    subnet_name = "idem-test-subnet-" + str(uuid.uuid4())
    resource_group_name = resource_group_fixture.get("name")
    virtual_network_name = virtual_network_fixture.get("name")
    subnet_parameters = {
        "location": "eastus",
        "properties": {
            "addressPrefix": "10.12.13.0/28",
        },
    }
    subnet_ret = await hub.states.azure.virtual_networks.subnets.present(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
        parameters=subnet_parameters,
    )
    if subnet_ret["result"]:
        subnet_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
            ctx,
            url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
            f"/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}/subnets/{subnet_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield subnet_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request subnet {subnet_name} with parameters {subnet_parameters} with "
            f"error: {subnet_ret['comment']} {subnet_ret.get('ret', '')}"
        )
    delete_ret = await hub.states.azure.virtual_networks.subnets.absent(
        ctx,
        name=subnet_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        subnet_name=subnet_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete subnet {subnet_name}")
    else:
        hub.log.debug(
            f"Failed to delete subnet {subnet_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="session")
async def network_security_group_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new network security group. This network security group gets deleted during test clean-up.
    """
    sg_name = "idem-test-network-security-group-" + str(uuid.uuid4())
    resource_group_name = resource_group_fixture.get("name")
    sg_parameters = {"location": "eastus"}
    sg_ret = await hub.states.azure.virtual_networks.network_security_groups.present(
        ctx,
        name=sg_name,
        resource_group_name=resource_group_name,
        network_security_group_name=sg_name,
        parameters=sg_parameters,
    )
    if sg_ret["result"]:
        sg_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
            ctx,
            url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{sg_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield sg_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request network security group {sg_name} with parameters {sg_parameters} with "
            f"error: {sg_ret['comment']} {sg_ret.get('ret', '')}"
        )
    delete_ret = await hub.states.azure.virtual_networks.network_security_groups.absent(
        ctx,
        name=sg_name,
        resource_group_name=resource_group_name,
        network_security_group_name=sg_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete network security group {sg_name}")
    else:
        hub.log.debug(
            f"Failed to delete network security group {sg_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )


@pytest.fixture(scope="session")
async def public_ip_address_fixture(hub, ctx, resource_group_fixture):
    """
    Fixture to create a new public ip address. This public ip address gets deleted during test clean-up.
    """
    pub_ip_name = "idem-test-public-ip-" + str(uuid.uuid4())
    resource_group_name = resource_group_fixture.get("name")
    pub_ip_parameters = {"location": "eastus"}
    pub_ip_ret = await hub.states.azure.virtual_networks.public_ip_addresses.present(
        ctx,
        name=pub_ip_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=pub_ip_name,
        parameters=pub_ip_parameters,
    )
    if pub_ip_ret["result"]:
        pub_ip_wait_ret = await hub.tool.azure.test_utils.wait_for_resource_to_present(
            ctx,
            url=f"https://management.azure.com/subscriptions/{ctx.acct.subscription_id}/resourcegroups/{resource_group_name}"
            f"/providers/Microsoft.Network/publicIPAddresses/{pub_ip_name}?api-version=2021-03-01",
            retry_count=10,
            retry_period=10,
        )
        yield pub_ip_wait_ret["ret"]
    else:
        raise RuntimeError(
            f"Fail to request public ip address {pub_ip_name} with parameters {pub_ip_parameters} with "
            f"error: {pub_ip_ret['comment']} {pub_ip_ret['ret']}"
        )
    delete_ret = await hub.states.azure.virtual_networks.public_ip_addresses.absent(
        ctx,
        name=pub_ip_name,
        resource_group_name=resource_group_name,
        public_ip_address_name=pub_ip_name,
    )
    if delete_ret["result"]:
        hub.log.debug(f"Successfully delete public ip address {pub_ip_name}")
    else:
        hub.log.debug(
            f"Failed to delete public ip address {pub_ip_name} with error:"
            f" {delete_ret['comment']} {delete_ret['ret']}. Manual clean-up may be needed."
        )
