import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
NIC_NAME = "my-nic"
RESOURCE_PARAMETERS = {
    "properties": {"privateIpAddress": "10.12.13.11"},
    "location": "eastus",
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network interfaces. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.virtual_networks.network_interfaces.present = (
        hub.states.azure.virtual_networks.network_interfaces.present
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
        "headers": {},
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.virtual_networks.network_interfaces",
        "headers": {},
    }

    def _check_get_parameters(_ctx, url, success_codes, headers):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, headers, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.virtual_networks.network_interfaces.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NIC_NAME,
        RESOURCE_PARAMETERS,
    )
    assert differ.deep_diff(dict(), expected_put.get("ret")) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_put.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network interface. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.virtual_networks.network_interfaces.present = (
        hub.states.azure.virtual_networks.network_interfaces.present
    )
    patch_parameter = {"tags": {"new-tag-key": "new-tag-value"}}

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }
    expected_patch = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
            "tags": {"new-tag-key": "new-tag-value"},
        },
        "result": True,
        "status": 200,
        "comment": f"Would update azure.virtual_networks.network_interfaces with parameters: {patch_parameter}.",
        "headers": {},
    }

    def _check_patch_parameters(_ctx, url, success_codes, headers, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        assert json == patch_parameter
        return expected_patch

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.patch.side_effect = _check_patch_parameters
    mock_hub.tool.azure.utils.patch_json_content.return_value = patch_parameter

    ret = await mock_hub.states.azure.virtual_networks.network_interfaces.present(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NIC_NAME, patch_parameter
    )
    assert differ.deep_diff(
        expected_get.get("ret"), expected_patch.get("ret")
    ) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_patch.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network interfaces. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.virtual_networks.network_interfaces.absent = (
        hub.states.azure.virtual_networks.network_interfaces.absent
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
        "headers": {},
    }

    def _check_get_parameters(_ctx, url, success_codes, headers):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.virtual_networks.network_interfaces.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NIC_NAME
    )
    assert ret.get("changes") is None
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert f"'{RESOURCE_NAME}' already absent" == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network interfaces. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.virtual_networks.network_interfaces.absent = (
        hub.states.azure.virtual_networks.network_interfaces.absent
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
        "headers": {},
    }

    def _check_delete_parameters(_ctx, url, success_codes, headers):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.virtual_networks.network_interfaces.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NIC_NAME
    )
    assert differ.deep_diff(expected_get.get("ret"), dict()) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_delete.get("comment") == ret.get("comment")


# TODO: add describe unit test
