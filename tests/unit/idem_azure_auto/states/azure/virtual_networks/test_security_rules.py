import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
NETWORK_SECURITY_GROUP_NAME = "my-security-group"
SECURITY_RULE_NAME = "my-security-rule"
RESOURCE_PARAMETERS = {
    "properties": {
        "protocol": "*",
        "sourceAddressPrefix": "10.0.0.0/8",
        "destinationAddressPrefix": "11.0.0.0/8",
        "access": "Deny",
        "destinationPortRange": "8080",
        "sourcePortRange": "*",
        "priority": 100,
        "direction": "Outbound",
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of security rules. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.virtual_networks.security_rules.present = (
        hub.states.azure.virtual_networks.security_rules.present
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
        "headers": {},
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.virtual_networks.security_rules",
        "headers": {},
    }

    def _check_get_parameters(_ctx, url, success_codes, headers):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, headers, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.virtual_networks.security_rules.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
        RESOURCE_PARAMETERS,
    )
    assert differ.deep_diff(dict(), expected_put.get("ret")) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_put.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of security rules. When a resource exists, 'present' will return the existing resource. Since
    Azure security rules doesn't support PATCH operation
    """
    mock_hub.states.azure.virtual_networks.security_rules.present = (
        hub.states.azure.virtual_networks.security_rules.present
    )
    # This patch parameter should be ignored by "present"
    patch_parameter = {"tags": {"new-tag-key": "new-tag-value"}}

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }

    mock_hub.exec.request.json.get.return_value = expected_get

    ret = await mock_hub.states.azure.virtual_networks.security_rules.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
        patch_parameter,
    )
    assert differ.deep_diff(
        expected_get.get("ret"), expected_get.get("ret")
    ) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_get.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of security rules. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.virtual_networks.security_rules.absent = (
        hub.states.azure.virtual_networks.security_rules.absent
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
        "headers": {},
    }

    def _check_get_parameters(_ctx, url, success_codes, headers):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.virtual_networks.security_rules.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
    )
    assert ret.get("changes") is None
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert f"'{RESOURCE_NAME}' already absent" == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of security rules. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.virtual_networks.security_rules.absent = (
        hub.states.azure.virtual_networks.security_rules.absent
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}/securityRules/"
            f"{SECURITY_RULE_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
        "headers": {},
    }

    def _check_delete_parameters(_ctx, url, success_codes, headers):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert SECURITY_RULE_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.virtual_networks.security_rules.absent(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        SECURITY_RULE_NAME,
    )
    assert differ.deep_diff(expected_get.get("ret"), dict()) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_delete.get("comment") == ret.get("comment")


# TODO: add describe unit test
