import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-role-assignment"
ROLE_ASSIGNMENT_NAME = "my-role-assignment"
ROLE_DEFINITION_ID = "b24988ac-6180-42a0-ab88-20f7382dd24c"
SCOPE = ""
RESOURCE_PARAMETERS = {
    "properties": {
        "roleDefinitionId": f"{SCOPE}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
        "principalId": "d93a38bc-d029-4160-bfb0-fbda779ac214",
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of role assignments. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.authorization.role_assignments.present = (
        hub.states.azure.authorization.role_assignments.present
    )
    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
        "headers": {},
    }
    expected_put = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.authorization.role_assignments",
        "headers": {},
    }

    def _check_get_parameters(_ctx, url, success_codes, headers):
        assert SCOPE in url
        assert ROLE_ASSIGNMENT_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, headers, json):
        assert SCOPE in url
        assert ROLE_ASSIGNMENT_NAME in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.authorization.role_assignments.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        ROLE_ASSIGNMENT_NAME,
        RESOURCE_PARAMETERS,
    )
    assert differ.deep_diff(dict(), expected_put.get("ret")) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_put.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of role assignments. When a resource exists, 'present' will return the existing resource.
     Since Azure role assignments doesn't support PATCH operation
    """
    mock_hub.states.azure.authorization.role_assignments.present = (
        hub.states.azure.authorization.role_assignments.present
    )
    # This patch parameter should be ignored by "present"
    patch_parameter = {"tags": {"new-tag-key": "new-tag-value"}}

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }

    mock_hub.exec.request.json.get.return_value = expected_get

    ret = await mock_hub.states.azure.authorization.role_assignments.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        ROLE_ASSIGNMENT_NAME,
        patch_parameter,
    )
    assert differ.deep_diff(
        expected_get.get("ret"), expected_get.get("ret")
    ) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_get.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of role assignments. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.authorization.role_assignments.absent = (
        hub.states.azure.authorization.role_assignments.absent
    )

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
        "headers": {},
    }

    def _check_get_parameters(_ctx, url, success_codes, headers):
        assert SCOPE in url
        assert ROLE_ASSIGNMENT_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.authorization.role_assignments.absent(
        ctx, RESOURCE_NAME, SCOPE, ROLE_ASSIGNMENT_NAME
    )
    assert ret.get("changes") is None
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert f"'{RESOURCE_NAME}' already absent" == ret.get("comment")


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of role assignments. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.authorization.role_assignments.absent = (
        hub.states.azure.authorization.role_assignments.absent
    )

    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {
            "id": f"{SCOPE}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
        "headers": {},
    }

    def _check_delete_parameters(_ctx, url, success_codes, headers):
        assert SCOPE in url
        assert ROLE_ASSIGNMENT_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.authorization.role_assignments.absent(
        ctx, RESOURCE_NAME, SCOPE, ROLE_ASSIGNMENT_NAME
    )
    assert differ.deep_diff(expected_get.get("ret"), dict()) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret.get("result")
    assert expected_delete.get("comment") == ret.get("comment")


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of role assignments.
    """
    mock_hub.states.azure.authorization.role_assignments.describe = (
        hub.states.azure.authorization.role_assignments.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.utils.get_uri_parameter_value_from_uri = (
        hub.tool.azure.utils.get_uri_parameter_value_from_uri
    )
    global SCOPE
    SCOPE = f"subscriptions/{ctx.acct.subscription_id}"
    resource_id = f"/{SCOPE}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}"
    expected_list = {
        "ret": {
            "value": [{"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS}]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
        "headers": {},
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.authorization.role_assignments.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.authorization.role_assignments.present" in ret_value.keys()
    expected_describe = [
        {"scope": SCOPE},
        {"role_assignment_name": ROLE_ASSIGNMENT_NAME},
        {"parameters": expected_list.get("ret").get("value")[0]},
    ]
    assert expected_describe == ret_value.get(
        "azure.authorization.role_assignments.present"
    )
