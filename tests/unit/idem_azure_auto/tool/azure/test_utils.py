from collections import OrderedDict


def test_patch_json_content_with_dict(hub, mock_hub):
    """
    Test hub.tool.azure.utils.patch_json_content method. The method generates a patch payload that only contains
    patchable values (specified in PATCH_PARAMETERS), and only contains a value when it is different from the
    original value
    """
    patch_parameters = {
        "identity": "identity",
        "kind": "kind",
        "sku": "sku",
        "tags": "tags",
    }

    old_values = {
        "location": "eastus",
        "kind": "BlockBlobStorage",
        "sku": {"name": "Premium_LRS", "test-new": "test-new-value"},
    }

    new_values = {
        "location": "eastus",
        "kind": "BlockBlobStorage",
        "sku": {"name": "Premium_LRS"},
        "tags": {"tag-key": "tag-value"},
    }

    mock_hub.tool.azure.utils.patch_json_content = (
        hub.tool.azure.utils.patch_json_content
    )

    patch_payload = mock_hub.tool.azure.utils.patch_json_content(
        patch_parameters, old_values, new_values
    )
    assert patch_payload == {"tags": {"tag-key": "tag-value"}}


def test_patch_json_content_with_force_update_parameters(hub, mock_hub):
    patch_parameters = {
        "tags": "tags",
    }

    force_update_parameters = {
        "tags": "tags",
    }

    old_values = {
        "location": "eastus",
        "tags": {"tag-key-1": "tag-value-1", "tag-key-2": "tag-value-2"},
    }

    new_values = {
        "location": "eastus",
        "tags": {"tag-key-1": "tag-value-1"},
    }

    mock_hub.tool.azure.utils.patch_json_content = (
        hub.tool.azure.utils.patch_json_content
    )

    patch_payload = mock_hub.tool.azure.utils.patch_json_content(
        patch_parameters, old_values, new_values, force_update_parameters
    )
    assert patch_payload == {"tags": {"tag-key-1": "tag-value-1"}}


def test_patch_json_content_with_unpatchable_param(hub, mock_hub):
    patch_parameters = {
        "identity": "identity",
        "sku": "sku",
        "tags": "tags",
    }

    old_values = {
        "location": "eastus",
        "unpatchable-key": "unpatchable-value",
        "kind": "BlockBlobStorage",
    }

    new_values = {
        "location": "eastus",
        "unpatchable-key": "unpatchable-new-value",
        "kind": "BlockBlobStorage",
    }

    mock_hub.tool.azure.utils.patch_json_content = (
        hub.tool.azure.utils.patch_json_content
    )

    patch_payload = mock_hub.tool.azure.utils.patch_json_content(
        patch_parameters, old_values, new_values
    )
    assert patch_payload == {}


def test_patch_json_content_with_nested_param(hub, mock_hub):
    patch_parameters = {
        "identity": "identity",
        "properties": {
            "extensionsTimeBudget": "extensionsTimeBudget",
            "hardwareProfile": "hardwareProfile",
        },
        "tags": "tags",
    }

    old_values = {
        "location": "eastus",
        "properties": {"hardwareProfile": {"vmSize": "Standard_D2s_v1"}},
    }

    new_values = {
        "location": "eastus",
        "properties": {
            "hardwareProfile": {"vmSize": "Standard_D2s_v3"},
            "extensionsTimeBudget": {
                "extensionsTimeBudget-key": "extensionsTimeBudget-value"
            },
            "unpatchable-key": "unpatchable-value",
        },
    }

    mock_hub.tool.azure.utils.patch_json_content = (
        hub.tool.azure.utils.patch_json_content
    )

    patch_payload = mock_hub.tool.azure.utils.patch_json_content(
        patch_parameters, old_values, new_values
    )
    assert patch_payload == {
        "properties": {
            "hardwareProfile": {"vmSize": "Standard_D2s_v3"},
            "extensionsTimeBudget": {
                "extensionsTimeBudget-key": "extensionsTimeBudget-value"
            },
        }
    }


def test_patch_json_content_with_nested_param_with_force_update_parameters(
    hub, mock_hub
):
    patch_parameters = {
        "identity": "identity",
        "properties": {
            "extensionsTimeBudget": "extensionsTimeBudget",
            "hardwareProfile": "hardwareProfile",
            "tags": "tags",
        },
    }

    force_update_parameters = {
        "properties": {
            "tags": "tags",
        },
    }

    old_values = {
        "location": "eastus",
        "properties": {
            "hardwareProfile": {"vmSize": "Standard_D2s_v1"},
            "tags": {"tag-key": "tag-value"},
        },
    }

    new_values = {
        "location": "eastus",
        "properties": {
            "hardwareProfile": {"vmSize": "Standard_D2s_v3"},
            "extensionsTimeBudget": {
                "extensionsTimeBudget-key": "extensionsTimeBudget-value"
            },
            "tags": {},
            "unpatchable-key": "unpatchable-value",
        },
    }

    mock_hub.tool.azure.utils.patch_json_content = (
        hub.tool.azure.utils.patch_json_content
    )

    patch_payload = mock_hub.tool.azure.utils.patch_json_content(
        patch_parameters, old_values, new_values, force_update_parameters
    )
    assert patch_payload == {
        "properties": {
            "hardwareProfile": {"vmSize": "Standard_D2s_v3"},
            "extensionsTimeBudget": {
                "extensionsTimeBudget-key": "extensionsTimeBudget-value"
            },
            "tags": {},
        }
    }


def test_get_uri_parameter_value_from_uri(hub, mock_hub):
    subscription_id = "12345678-1234-1234-1234-aaabc1234aaa"
    resource_group_name = "my-resource-group"
    virtual_network_name = "my-vnet"
    uri_parameters = OrderedDict(
        {
            "resourceGroups": "resource_group_name",
            "virtualNetworks": "virtual_network_name",
        }
    )
    uri = (
        f"/subscriptions/{subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network"
        f"/virtualNetworks/{virtual_network_name}"
    )

    mock_hub.tool.azure.utils.get_uri_parameter_value_from_uri = (
        hub.tool.azure.utils.get_uri_parameter_value_from_uri
    )

    uri_values = mock_hub.tool.azure.utils.get_uri_parameter_value_from_uri(
        uri, uri_parameters
    )
    assert uri_values == [
        {"resource_group_name": resource_group_name},
        {"virtual_network_name": virtual_network_name},
    ]
