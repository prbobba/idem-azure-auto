import pytest
from dict_tools import data


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="session")
def ctx():
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    fake_subscription = "12345678-1234-1234-1234-aaabc1234aaa"
    fake_client_id = "76543210-4321-4321-4321-bbbb3333aaaa"
    fake_secret = "ZzxxxXXXX11xx-aaaaabbbb-k3xxxxxx"
    tenant = "bbbbbca-3333-4444-aaaa-cddddddd6666"
    yield data.NamespaceDict(
        run_name="unit-test-run",
        test=False,
        acct=data.NamespaceDict(
            endpoint_url=f"https://management.azure.com/subscriptions/{fake_subscription}",
            client_id=fake_client_id,
            secret=fake_secret,
            subscription_id=fake_subscription,
            tenant=tenant,
            headers=dict(),
        ),
    )
